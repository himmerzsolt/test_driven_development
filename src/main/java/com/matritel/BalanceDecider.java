package com.matritel;

import java.util.HashMap;
import java.util.Stack;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

public class BalanceDecider {
    private static final Pattern VALID_PATTERN = compile("[(|)\\[\\]{}]*");

    public boolean isBalanced(String input) {

        HashMap<Character,Character> map = new HashMap<>();
        map.put('(',')');
        map.put('[',']');
        map.put('{','}');
        if (input == null || !VALID_PATTERN.matcher(input).matches()) {
            throw new IllegalArgumentException();
        }
        Stack<Character> stack = new Stack();
        if ((input.startsWith(")") || input.startsWith("]") || input.startsWith("}"))) {
            return false;
        }

        else {
            for (int i = 0; i < input.length(); i++) {
                char c = input.charAt(i);
                if ((stack.empty()) && (input.charAt(i)==(')') || input.charAt(i)==(']') || input.charAt(i)==('}'))) {return false;}
                if (c == '(' || c == '{' || c == '[') {
                    stack.push(c);
                } else {
                    if (map.get(stack.peek()) == input.charAt(i)) {
                        stack.pop();
                    } else break;

                }
            }
        }
        if (stack.empty()) {return true;}
        else return false;
    }
}
