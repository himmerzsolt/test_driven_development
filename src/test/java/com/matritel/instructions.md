## TDD Workshop

##### 2018. nov 14. Wednesday

### BALANCED PARANTHENSES

- Write a program to determine if the the parentheses (),
the brackets [], and the braces {}, in a string are balanced.

_Valid inputs include only the **combination** following characters:_
- '('
- ')'
- '['
- ']'
- '{'
- '}'

_For example:_

**{{)(}}** is not balanced because **)** comes before **(**

**({)}** is not balanced because **)** is not balanced between **{}**
     and similarly the **{** is not balanced between **()**

**[({})]** is balanced

**{}([])** is balanced

**{()}[[{}]]** is balanced

**{[a]}** is an invalid input