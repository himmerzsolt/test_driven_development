package com.matritel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BalanceDeciderTest {
    private BalanceDecider underTest;

    @Before
    public void setUp () {
        underTest = new BalanceDecider();
    }
    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenInputIsNull(){
        underTest.isBalanced(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenInputContainsInvalidCharacter() {
        String input = "invalid input";
        underTest.isBalanced(input);
    }

    @Test
    public void shouldReturnTrueWhenInputIsBalanced() {
        String input = "[]";
        boolean actual = underTest.isBalanced(input);
        assertTrue(actual);
    }

    @Test
    public void shouldReturnFalseWhenInputsLengthIsOdd() {
        String input = "[";
        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);
    }

    @Test
    public void shouldReturnFalseWhenInputStartsWithClosingBracket() {
        String input = "][";
        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);
    }

    @Test
    public void shouldReturnFalseWhenInputEndsWithOpeningBracket() {
        String input = "[[";
        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);

    }

    @Test
    public void shouldReturnFalseIfBracketDoesNotHavePair() {
        String input = "({))";

        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);

    }

    @Test
    public void shouldReturnFalseWhenBracketsAreNotInProperOrder() {
        String input = "{){(}}";
        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);
    }
    @Test
    public void shouldReturnFalseWhenStackIsEmptyAndNextCharacterInStringIsClosingBracket(){
        String input = "[]][{]{(({{)[})(}[[))}{}){[{]}{})()[{}]{{]]]){{}){({(}](({[{[{)]{)}}}({[)}}([{{]]({{";
        boolean actual = underTest.isBalanced(input);
        assertFalse(actual);
    }
}
